package com.example.assignmentsix

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import com.example.assignmentsix.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding : ActivityMainBinding

    val user = User(
        "Ana",
        "Berulava",
        "anna6berulava@gmail.com",
        "Femail",
        "1997")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initFields()

        binding.btnSaveInfo.setOnClickListener {
            openUserInfoDashBoard()
        }
    }

    val construct = registerForActivityResult(ActivityResultContracts.StartActivityForResult())
    {
        if(it.resultCode == RESULT_OK)
        {
            //resultInfo is null but?? why?
           val resultInfo = intent.getParcelableExtra<User>(UpdateInfoActivity.userInfo.USER)

            binding.etFirstName.setText(resultInfo?.firstName)
            binding.etLastName.setText(resultInfo?.lastName)
            binding.etEmail.setText(resultInfo?.email)
            binding.etGender.setText(resultInfo?.gender)
            binding.etBirthYear.setText(resultInfo?.birthYear)

        }
    }

    private fun openUserInfoDashBoard()
    {
        val intent = Intent(this,UpdateInfoActivity::class.java)
        intent.putExtra(UpdateInfoActivity.userInfo.USER,user)
        construct.launch(intent)
    }


    private fun initFields()
    {

        binding.etFirstName.setText(user.firstName)
        binding.etLastName.setText(user.lastName)
        binding.etEmail.setText(user.email)
        binding.etGender.setText(user.gender)
        binding.etBirthYear.setText(user.birthYear)


    }
}