package com.example.assignmentsix

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.contract.ActivityResultContract
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.result.contract.ActivityResultContracts.StartActivityForResult
import androidx.appcompat.app.AppCompatActivity
import com.example.assignmentsix.databinding.ActivityUpdateInfoBinding


class UpdateInfoActivity : AppCompatActivity() {

    private lateinit var binding:ActivityUpdateInfoBinding

    object userInfo
    {
        const val USER = "com.example.assignmentsix.user"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUpdateInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getResultfromFirstActivity()

        binding.btnSaveInfo.setOnClickListener {

            backToUserInfoActivity()
        }
    }


    private fun getResultfromFirstActivity()
    {
        val user = intent.getParcelableExtra<User>(userInfo.USER)

        binding.etFirstName.setText(user?.firstName)
        binding.etLastName.setText(user?.lastName)
        binding.etEmail.setText(user?.email)
        binding.etGender.setText(user?.gender)
        binding.etBirthYear.setText(user?.birthYear)
    }


    private fun backToUserInfoActivity()
    {
        val updatedUserInfo = User(
            binding.etFirstName.text.toString(),
            binding.etLastName.text.toString(),
            binding.etEmail.text.toString(),
            binding.etGender.text.toString(),
            binding.etBirthYear.text.toString()
        )

        val intent = intent
        intent.putExtra(userInfo.USER,updatedUserInfo)
        setResult(Activity.RESULT_OK,intent)
        finish()

    }
}